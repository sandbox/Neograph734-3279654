((Drupal, settings, once) => {

  // Todo: No idea yet what this should do. For now update an iframe.
  function preview(event) {
    const colors = event.detail.colors;
    let style = '';
    Object.entries(colors).forEach(color => {
      const [key, value] = color;
      style += `--color-${key}: ${value};`;
    });
    const iFrameBody = frames['color-poc-preview-frame'].document;
    iFrameBody.querySelector('html').setAttribute('style', style)
  }

  Drupal.behaviors.colorPocPreview = {
    attach: () => {
      const formElements = once('color-poc', '[data-drupal-selector="edit-color-poc"]');
      formElements.forEach(form => {
        form.addEventListener('color-poc-changed', preview);
      });
    }
  };
})(Drupal, drupalSettings, once);
