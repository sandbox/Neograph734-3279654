((Drupal, settings, once) => {
  /**
   * Initializes the color scheme selection element (selectElement).
   * @param selectElement - The element to initialize.
   */
  function initColorSchemeSelect(selectElement) {
    // Update the colors elements if the selectElement's value changes.
    selectElement.addEventListener('change', setColorScheme);
    // Iterate over all color schemes and see if the current color values match
    // with a known scheme. If so, set the selectElement's value to the scheme.
    Object.entries(settings.color_poc.schemes).forEach(option => {
      const [scheme, values] = option;
      const {label, colors} = values;
      let allColorsMatch = true;
      Object.entries(colors).forEach(_ref3 => {
        let [colorName, colorHex] = _ref3;
        const field = document.querySelector(`input[type="color"][name="color_poc[colors][${colorName}]"]`);
        if (field.value !== colorHex) {
          allColorsMatch = false;
        }
      });

      if (allColorsMatch) {
        selectElement.value = scheme;
      }
    });
  }

  /**
   * Updates all color elements once a preset color scheme is chosen.
   * @param event - The change event.
   */
  function setColorScheme(event) {
    let {
      target
    } = event;
    if (!target.value) return;
    const selectedColorScheme = settings.color_poc.schemes[target.value].colors;
    if (selectedColorScheme) {
      Object.entries(selectedColorScheme).forEach(scheme => {
        let [key, color] = scheme;
        document.querySelectorAll(`input[name="color_poc[colors][${key}]"]`).forEach(input => {
          if (input.value !== color) {
            input.value = color;
          }
        });
      });
      dispatchChangedEvent();
    }
  }

  /**
   * Changes the selectElement to have an empty value.
   */
  function unsetSchemeSelect() {
    const selectElement = document.querySelector('[data-drupal-selector="edit-color-poc"] select')
    if (selectElement.value !== '') {
      selectElement.value = '';
    }
  }

  /**
   * Dispatches a custom 'color-poc-changed' event with the new color array in
   * the event details. This allows other JavaScripts to listen to updating
   * colors.
   */
  function dispatchChangedEvent() {
    let colorArray = [];
    document.querySelectorAll('[data-drupal-selector="edit-color-poc"] input[type="color"]').forEach(colorInput => {

      // Convert the nested form name into the property name.
      let colorName = colorInput.name;
      const index = colorName.lastIndexOf('[');
      colorName = colorName.slice(index + 1, -1);

      colorArray[colorName] = colorInput.value;
    });
    const eventChanged = new CustomEvent('color-poc-changed', { detail: { colors: colorArray } });
    document.querySelector('[data-drupal-selector="edit-color-poc"]').dispatchEvent(eventChanged);
  }

  Drupal.behaviors.colorPoc = {
    attach: () => {
      const colorSchemeSelect = once('color-poc', '[data-drupal-selector="edit-color-poc"] select');
      colorSchemeSelect.forEach(selectElement => {
        initColorSchemeSelect(selectElement);
      });
      const colorTextInputs = once('color-poc', '[data-drupal-selector="edit-color-poc"] input[type="color"]');
      colorTextInputs.forEach(colorInput => {
        colorInput.addEventListener('input', unsetSchemeSelect);
        colorInput.addEventListener('input', dispatchChangedEvent);
      });

      // Allow the preview to update with the values loaded from configuration.
      dispatchChangedEvent();
    }
  };
})(Drupal, drupalSettings, once);
