<?php

/**
 * Allows themes and other modules to alter color definitions.
 *
 * Before passing the colors to the page, this hook will allow theme designers
 * to add derived (lighter, darker) colors to the page. There are multiple PHP
 * libraries that can be utilized for this. For example one could use the
 * Iris - PHP Color Library.
 *
 * @see https://github.com/ozdemirburak/iris
 *
 * @code
 * function HOOK_color_poc_alter_colors(&$variables) {
 *   $colors['base-primary-color-light'] = \OzdemirBurak\Iris\Color\Hex($colors['base-primary-color'])->lighten(20);
 * }
 * @endcode
 *
 * @param $colors
 *   An array of colors. The key is the color variable name and the value being
 *   the hex code of the color.
 */
function hook_color_poc_alter_colors(&$colors) {
  // Add additional colors to the array.
  $colors['base-secondary-color'] = '#8f45a8';
}
