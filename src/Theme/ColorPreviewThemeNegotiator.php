<?php

namespace Drupal\color_poc\Theme;

use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;

/**
 * Allows rendering of the front page in a different theme for color previews.
 */
class ColorPreviewThemeNegotiator implements ThemeNegotiatorInterface {

  /**
   * The theme handler.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  var $themeHandler;

  /**
   * Constructs a ColorPreviewThemeNegotiator.
   *
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $themeHandler
   */
  public function __construct(ThemeHandlerInterface $themeHandler) {
    $this->themeHandler = $themeHandler;
  }

  /**
   * Gets the theme.
   *
   * @return string
   *   The theme system name if found, or NULL otherwise.
   */
  protected function getTheme() {
    // @todo: Perhaps try to find a better way to manage this. The query
    //   parameter is an easy way to change the theme, but it feels ugly.
    $theme_from_url = \Drupal::request()->query->get('color_poc_preview_theme');

    // Check if this is an existing theme and if it is enabled.
    if ($this->themeHandler->themeExists($theme_from_url)) {
      return $theme_from_url;
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    // Do not allow people to change the appearance of the site by using query
    // parameters. At least do some permission checking.
    if (\Drupal::currentUser()->hasPermission('administer themes') && $this->getTheme()) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $route_match) {
    return $this->getTheme();
  }

}
