<?php

namespace Drupal\color_poc;

use Drupal\Core\Extension\ThemeExtensionList;
use Drupal\Core\Theme\ThemeColorsParser;

/**
 * A color fetcher for obtaining the modifiable colors in themes.
 */
class ColorFetcher {

  /**
   * Associative array of available colors per theme, keyed by theme name.
   *
   * @var array
   */
  private $themesColors;

  /**
   * The theme extension list.
   *
   * @var \Drupal\Core\Extension\ThemeExtensionList
   */
  private $themeExtensionList;

  /**
   * Constructs a ColorFetcher.
   *
   * @param \Drupal\Core\Extension\ThemeExtensionList $theme_extension_list
   *   The theme extension list.
   */
  public function __construct(ThemeExtensionList $theme_extension_list) {
    $this->themeExtensionList = $theme_extension_list;
  }

  /**
   * Fetches the colors that can be changed in a theme.
   *
   * @param string $theme
   *   The theme system name to get the colors from.
   *
   * @return array
   *   An array with color variables and color schemes from the theme.
   */
  public function fetch($theme) {
    // Only lookup the value if it is undefined.
    if (!isset($this->themesColors[$theme])) {
      $parser = new ThemeColorsParser();
      $file = $this->themeExtensionList->getPath($theme) . "/$theme.colors.yml";
      $this->themesColors[$theme] = $parser->parse($file);
    }
    return $this->themesColors[$theme];
  }

}
