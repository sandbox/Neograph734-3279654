<?php

namespace Drupal\color_poc\Form;

use Drupal\color_poc\ColorFetcher;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ColorForm extends ConfigFormBase {

  /**
   * The color fetcher.
   *
   * @var \Drupal\color_poc\ColorFetcher
   */
  private $colorFetcher;

  /**
   * An array of configuration names that should be editable.
   *
   * @var array
   */
  protected $editableConfig = [];

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\color_poc\ColorFetcher $color_fetcher
   *   The color fetcher.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ColorFetcher $color_fetcher) {
    parent::__construct($config_factory);

    $this->colorFetcher = $color_fetcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('color_poc.fetcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return $this->editableConfig;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'color_poc_settings';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $theme = '') {
    $config_key = $theme . '.settings';
    $this->editableConfig = [$config_key];

    // Define $colors and $schemes.
    ['colors' => $colors, 'schemes' => $schemes] = $this->colorFetcher->fetch($theme);

    $form['color_poc'] = [
      '#type' => 'details',
      '#title' => t('Color scheme'),
      '#open' => TRUE,
      '#tree' => TRUE,
      '#attributes' => [
        'id' => 'color_poc_scheme_form',
      ],
      '#attached' => [
        'library' => [
          'color_poc/color_poc.admin',
        ],
        'drupalSettings' => [
          'color_poc' => [
            'schemes' => $schemes
          ],
        ],
      ],
    ];

    $form['color_poc']['scheme'] = [
      '#type' => 'select',
      '#title' => $this->t('Color scheme'),
      '#options' => array_combine(array_keys($schemes), array_column($schemes, 'label')),
      '#empty_option' => $this->t('Custom'),
      '#empty_value' => NULL,
    ];

    $form['color_poc']['colors'] = [
      '#type' => 'container',
      '#title' => $this->t('Colors'),
    ];
    foreach ($colors as $variable => $label) {
      // If the color is not yet configured, try to fetch it from the first
      // color scheme.
      $fallback_color = array_values($schemes)[0]['colors'][$variable] ?? NULL;
      $form['color_poc']['colors'][$variable] = [
        '#type' => 'color',
        '#title' => $label,
        '#default_value' => $this->color_poc_palette_color_value($variable, $fallback_color),
      ];
    }

    // @todo: Keep an eye on change record https://www.drupal.org/node/3264760
    $form['color_poc']['preview'] = [
      '#type' => 'html_tag',
      '#tag' => 'iframe',
      '#attributes' => [
        // The path contains the color_poc_preview_theme query parameter,
        // allowing this module's theme negotiator to force a theme for the
        // preview within the iFrame. This renders the front page in the same
        // theme this settings page is about.
        'src' => \Drupal::urlGenerator()->generateFromRoute('<front>', [], ['absolute' => TRUE, 'query' => ['color_poc_preview_theme' => $theme]]),
        'allow' => 'fullscreen',
        'width' => 1200,
        'height' => 800,
        'loading' => 'eager',
        'name' => 'color-poc-preview-frame',
        'style' => 'zoom: 0.8',
      ],
      // Lower the weight so it renders at the top. Float right from the CSS.
      '#weight' => -10,
      '#attached' => [
        'library' => [
          'color_poc/color_poc.preview',
        ],
      ],
    ];

    return $form;
  }

  /**
   * Determines the value for a color field.
   *
   * @return string
   *   The HEX color value to set for the field.
   */
  public function color_poc_palette_color_value($key, $fallback_value) {
    $color_settings = $this->config($this->editableConfig[0])->get('third_party_settings.color_poc.colors');
    if (!empty($color_settings[$key])) {
      return $color_settings[$key];
    }
    return $fallback_value;
  }

}
